# Project : venom.hack.GoogleDorkFile

	# Project : venom.hack.GoogleDorkFile
	# Version : Public.Git.V0
	# Dependency / Dependencies : none
	# Language : English
	# License : GPL V3.0 - https://www.gnu.org/licenses/gpl-3.0.html
	# Author : @SecureSh3ll (Tests Mac/OsX by hadrienaka - github.com/hadrienaka)

## Description 

Generate a google dork code to find files hidden in servers on the internet.

## Usage 

```chmod u+x venom.hack.GoogleDorkFile && ./venom.hack.GoogleDorkFile```

Another method of execution :

```bash venom.hack.GoogleDorkFile```

### Support : 
+ GNU/Linux 
+ MacOS
